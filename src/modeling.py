import pandas as pd
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
from pycaret.classification import *

#########################################################################################
#                                                                                       #
# Read training, validation and test data                                               #
# Columns of the tables are IGFBP7 (ng/ml),SSC5D (ng/ml), SEMA4D (ug/ml), FibrosisStage #
#                                                                                       #    
#########################################################################################

training_data = pd.read_csv("training_data.csv") 
val_data = pd.read_csv("validation_data.csv")
test_data = pd.read_csv("test_data.csv") 

lda = LinearDiscriminantAnalysis()
lda.fit(training_data.iloc[:,:-1],training_data.iloc[:,-1])
print(f"LDA accuracy: {lda.score(training_data.iloc[:,:-1],training_data.iloc[:,-1]):.2f}")

lds_train = lda.transform(training_data.iloc[:,:-1])
training_data["LD1"] = lds_train[:,0]
training_data["LD2"] = lds_train[:,1]

lds_val = lda.transform(val_data.iloc[:,:-1])
val_data["LD1"] = lds_val[:,0]
val_data["LD2"] = lds_val[:,1]

experiment = setup(training_data,test_data=val_data,target="FibrosisStage",normalize=False,fold=10,verbose=False,index=False)
lgbm = create_model("lightgbm")
tuned_lgbm = tune_model(lgbm,n_iter=1000)
save_model(tuned_lgbm, 'tuned_lgbm')