# TLM3 Fibrosis Prediction 
## About
This repository contains the code for training and testing of machine learning model that was developed for predicting the fibrosis stage from blood biomarkers. For more information, you can check out the manuscript,  _Development of a novel non-invasive biomarker panel for hepatic1 fibrosis in MASLD (Verschuren et al 2024)_. 
## Instructions 
To run the code, first you need to install the dependencies. Codebase is Python, thus you can use the `env.yml` file to install the dependencies either with `conda` or `mamba` by using `conda create -f env.yml` command. 

You can either run the scripts seperately inside the `src` folder or copy them to a Jupyter notebook to run them interactively.